import json
from dataclasses import dataclass
from pathlib import Path
from typing import Dict, List

import click
import requests
from click.core import Context

from magyc.common import (build_key_from_parameters, do_download, 
                          find_config, get_group, get_mime_type, resolve_keys,
                          CmdCtx)

DEFAULT_URL = 'http://idoc-magyc-rec.ias.u-psud.fr:8080/'
DEFAULT_DIRECTORY = './tmp'




@click.group()
@click.option('-g', '--group', 'groups',
              type=str, multiple=True, required=True,
              help="Group uuids to save. \nMultiple occurrence '-g id1 -g id2' \nor comma separated values '-g id1,id2")
@click.option('-u', '--url',
              type=str,
              default=DEFAULT_URL,
              help=f"Magyc Front URL.\nDefault is {DEFAULT_URL}")
@click.option('-d', '--directory',
              type=str, default=DEFAULT_DIRECTORY,
              help=f"Directory location.\nDefault is {DEFAULT_DIRECTORY}")
@click.option('-k', '--key', 'keys',
              type=str, multiple=True,
              help="Parameter to use for building cache key. \nMultiple occurrence '-k id1 -k id2' \nor comma separated values '-k id1,id2")
@click.option('-c', '--config', 'config_file',
              type=str,
              help="keyConfig file")              
@click.pass_context
def cli(ctx: Context, groups: List[str], url: str, directory: str, keys: List[str] = None, config_file: str=None):
    click.echo("Magyc Toolset")

    config = find_config(config_file)

    key_config : Dict[str, List[str]]
    key_config = config['keys'] if 'keys' in config else {}

    if directory == DEFAULT_DIRECTORY and 'directory' in config:
        directory = config['directory']

    if url == DEFAULT_URL and 'url' in config:
        url = config['url']

    if len(groups) == 1:
        groups = [g.strip() for g in groups[0].split(',')]
    if keys and len(keys) == 1:
        keys = [g.strip() for g in keys[0].split(',')]

    ctx.obj = CmdCtx(groups=groups, 
                     url=url,
                     directory=directory, keys=keys if keys else [],
                     key_config= key_config )


@cli.command(help="Download MAGYC group's products")
@click.pass_context
def download(ctx):
    """
    Download artefacts
    """
    c: CmdCtx = ctx.obj

    do_download(c)

@cli.command(help="Infos")
@click.pass_context
def infos(ctx):
    """
    Get infos artefacts
    """
    c: CmdCtx = ctx.obj

    fmt_head = '[ {0:<25} ] {1:<25} ]'
    fmt_details = '| {0:<25} | {1}'    

    for group_id in c.groups:
        group = get_group(c.url, group_id)

        for job in group:
            job_name = job['jobList']
            keys = resolve_keys(job_name, c.keys, c.key_config)
            key = build_key_from_parameters(job_name, job['parameters'], keys)
            print()
            print('-'*len(job_name), '-'*len(key))
            print(fmt_head.format(job_name, key))
            print('-'*len(job_name), '-'*len(key))

            print(fmt_details.format('Artefact', 'URL'))
            print(fmt_details.format('--------', '---'))
            for result in job['results']:
                _, extension = get_mime_type(result['mimeType'])
                href = result['href']
                name = result['id']
                print(fmt_details.format(name, href))


if __name__ == "__main__":
    cli()

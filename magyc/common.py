import json
from pathlib import Path
from dataclasses import dataclass
from typing import Dict, List
import requests
from requests.models import Response
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)



@dataclass
class CmdCtx:
    groups: List[str]
    url: str
    directory: str
    keys: List[str]
    key_config:  Dict[str,List[str]]


def resolve_keys(job_name: str, keys: List[str]=None, key_config: Dict[str, List[str]]=None):
    """Resolves key names from parameters

    Args:
        job_name (str)
        keys (List[str], optional): List of parameters. Defaults to None.
        key_config (Dict[str, List[str]], optional): Table of parameters per job. Defaults to None.

    Returns:
        keys: List of names
    """
    if job_name in key_config:
        return key_config[job_name]
    elif '*' in key_config:
        return key_config['*']
    return keys


def build_key_from_parameters(job_name: str, parameters, names=None):
    cleaned_params = {k: v for k, v in parameters.items() if v not in [
        '', '0', '0.0', '-1', 'false']}
    if 'coordframe' in cleaned_params:
          del cleaned_params['coordframe']
    if names:
        key_to_remove = [k for k in cleaned_params.keys() if k not in names]
        for k in key_to_remove:
            del cleaned_params[k]
    return forge_key(job_name, cleaned_params)


def forge_key(job_name: str, params):
    return job_name.upper()+"-"+"-".join([f'{k}[{v}]' for k, v in sorted(params.items())])


def get_group(base_url: str, group_id: str):
    """Get group object from REST service /group/{groupId}

    Args:
        base_url (str): The Magyc front URL
        group_id (str): The group id

    Returns:
        [dict]: The Magyc Group object
    """
    r = requests.get(f'{base_url}group/{group_id}', verify=False)
    return r.json()


def get_json_product(direct_url: str):
    r = requests.get(direct_url, verify=False)
    return r.json()


def save_file(save_dir: Path, name: str, extension: str, result: Response):
    """Save file to disk

    Args:
        save_dir (Path): Directory location
        name (str): Product's name
        extension (str): Product's extension
        result (Response): The request response to UWS request to get the result
    """
    name_path = save_dir / Path(name)
    name_path = name_path.with_suffix(f'.{extension}')
    if extension == 'json':
        with open(name_path, 'w') as outfile:
            json.dump(result.json(), outfile, indent=4)
    else:
        with open(name_path, 'wb') as outfile:
            outfile.write(result.content)


def create_empty_save_dir(base_save_dir: Path, key: str):
    """Create save directory if it doesn't exist. Remove its content if it exists.

    Args:
        base_save_dir (Path): The base save dir location
        key (str): The child folder from base_save_dir

    Returns:
        Path: The created save dir
    """
    save_dir = base_save_dir / Path(key)
    if save_dir.exists():
        for f in save_dir.iterdir():
            if f.is_file():
                f.unlink()
        save_dir.rmdir()
    save_dir.mkdir(parents=True)
    return save_dir


def get_mime_type(mime_type: str):
    return tuple(mime_type.lower().split('/'))

def save_job_description(save_dir, job) :
    name_path = save_dir / Path('__job__.json')
    with open(name_path, 'w') as outfile:
        json.dump(job, outfile, indent=4)

def find_config(config_file: str=None):

    config = {}
    _path: Path

    if not config_file:
        _path =  Path.cwd() / Path('magyc-products-config.json')
    else:
        _path = Path(config_file)

    if Path.exists(_path):
        with open(_path, 'r') as f:
            config =  json.load(f)

    return config


def do_download(c: CmdCtx):
    """Does the download process

    Args:
        c (CmdCtx): Command Context
    """
    base_save_dir = Path(c.directory)

    for group in c.groups:
        group = get_group(c.url, group)

        if not check_jobs_are_completed(group):
            return

        for job in group:
            job_name = job['jobList']

            keys = resolve_keys(job_name, c.keys, c.key_config)
            key = build_key_from_parameters(job_name, job['parameters'], keys)
            save_dir = create_empty_save_dir(base_save_dir, key)
            print(F"Save dir: {save_dir}")
            save_job_description(save_dir, job)

            for result in job['results']:
                _, extension = get_mime_type(result['mimeType'])
                href = result['href']
                if not 'http' in href: 
                    url = c.url[:-1] if c.url[-1] == '/' else c.url
                    href= f'{url}{href}'
                name = result['id']
                print(f'Save {name:>25} from {href}')

                r = requests.get(href, verify=False)
                save_file(save_dir, name, extension, r)

def check_jobs_are_completed(group) -> bool :
    status_by_job = { job['jobList']: job['phase'] for job in group }
    status = {v for v in status_by_job.values() }
    if len(status) > 1 or 'COMPLETED' not in status:
        print('All job are not completed ', status_by_job)
        return False
    return True

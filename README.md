# Magyc Toolset for MAGYC products

## Install

* Clone project `git clone git@git.ias.u-psud.fr:MAGYC/magyc-products.git`
* Create a virtual environment

```
$ cd magyc-products/
$ python3 -m venv .venv --prompt "Magyc Products"
```

* Install dependencies

```bash
$. .venv/bin/activate
$ pip install -r requirements.txt
```

## Magyc CLI

Magyc CLI is the Command Line Interface to use the software. Use `--help` to get help.

```bash
$ python magycli.py --help
Usage: magycli.py [OPTIONS] COMMAND [ARGS]...

Options:
  -g, --group TEXT      Group uuids to save.  Multiple occurrence '-g id1 -g
                        id2'  or comma separated values '-g id1,id2
                        [required]

  -u, --url TEXT        Magyc Front URL. Default is http://idoc-magyc-
                        rec.ias.u-psud.fr:8080/

  -d, --directory TEXT  Directory location. Default is ./tmp
  -k, --key TEXT        Parameter to use for building cache key.  Multiple
                        occurrence '-k id1 -k id2'  or comma separated values
                        '-k id1,id2

  -c, --config PATH     keyConfig file
  --help                Show this message and exit.

Commands:
  download  Download MAGYC group's products
  infos     Infos
```

## Download

To download products use `download` command

```bash
python magycli.py download --help
Magyc Toolset
Usage: magycli.py download [OPTIONS]

  Download MAGYC group's products

Options:
  -g, --group TEXT      Group uuids to save.  Multiple occurrence '-g id1 -g
                        id2'  or comma separated values '-g id1,id2
                        [required]

  -u, --url TEXT        Magyc Front URL. Default is http://idoc-magyc-
                        rec.ias.u-psud.fr:8080/

  -d, --directory TEXT  Directory location. Default is ./tmp
  -k, --key TEXT        Parameter to use for building cache key.  Multiple
                        occurrence '-k id1 -k id2'  or comma separated values
                        '-k id1,id2

  --help                Show this message and exit.
```

To download all product link to a given group, use `download` command.

```bash
$ magycli.py -g 5cfc9227-774f-11eb-b06c-15f9e3431a84 -k ra,dec,radius download
Magyc Toolset
Save dir: tmp/XMATCHCATVO-dec[34.0511]-ra[255.666]-radius[10.0]
Save                      data from http://idoc-magyc-rec.ias.u-psud.fr:8080/proxy/ias/uws/xmatchcatvo/1614246831156/results/data
Save                   display from http://idoc-magyc-rec.ias.u-psud.fr:8080/proxy/ias/uws/xmatchcatvo/1614246831156/results/display
Save               xmatchCatVO from http://idoc-magyc-rec.ias.u-psud.fr:8080/proxy/ias/uws/xmatchcatvo/1614246831156/results/xmatchCatVO
Save dir: tmp/SIMPLE_MOCK_CHECK-dec[34.0511]-ra[255.666]-radius[10.0]
Save                      data from http://idoc-magyc-rec.ias.u-psud.fr:8080/proxy/ias/uws/simple_mock_check/1614246831198/results/data
Save                   display from http://idoc-magyc-rec.ias.u-psud.fr:8080/proxy/ias/uws/simple_mock_check/1614246831198/results/display
Save     Out_simple_xray_check from http://idoc-magyc-rec.ias.u-psud.fr:8080/proxy/ias/uws/simple_mock_check/1614246831198/results/Out_simple_xray_check
...
```

All files are downloaded to _./tmp/_

**The directory is created or erased for each run !**

```bash
ls -1
'SIMPLE_MOCK_CHECK-dec[34.0511]-ra[255.666]-radius[10.0]'
'SZ_ANALYSIS-dec[34.0511]-ra[255.666]-radius[10.0]'
'OPTICAL_ANALYSIS-dec[34.0511]-ra[255.666]-radius[10.0]'
'XMATCHCATVO-dec[34.0511]-ra[255.666]-radius[10.0]'
'XRAY_ANALYSIS-dec[34.0511]-ra[255.666]-radius[10.0]'
```

```bash
ls -l SIMPLE_MOCK_CHECK-dec\[34.0511\]-ra\[255.666\]-radius\[10.0\]/
total 16
-rw-rw-r-- 1 cram cram  380 févr. 26 10:58 data.json
-rw-rw-r-- 1 cram cram  218 févr. 26 10:58 display.json
-rw-rw-r-- 1 cram cram 2313 févr. 26 10:58 __job__.json
-rw-rw-r-- 1 cram cram 2150 févr. 26 10:58 Out_simple_xray_check.json
```

The job is saved as `__job__.json` into the directory.


### Directory

Directory is built on parameters, removing default values as

```
'', '0', '0.0', '-1', 'false'
````

To avoid useless parameters, you could specify the used one to build the directory name with the `-k` option

```bash
$ python magycli.py -g fc0c2ab1-7037-11eb-8aa7-cbfc2ae36756 -k ra,dec,radius download
Magyc Toolset
Save dir: tmp/SIMPLE_MOCK_CHECK-dec[34.0511]-ra[255.666]-radius[10.0]
...
```

Folder pattern is

```
{job_name.uppercase}-param1[value]-param2[value]...
```

****Job names are ever kept.****

## Configuration File

You could use a configuration file to put parameters as

- directory
- url
- keys

The provided values in file will override the default ones. But specified values passed as argument will be prioritized.


```json
{
  "directory": "/var/lib/magyc-cache",
  "keys": {
      "*": [
          "ra",
          "dec",
          "radius"
      ]
  }
}

```

### Auto find

If a file named `magyc-products-config.json` is found in the current directory, it will be used as a config file 

### Keys

You could use a keyConfig file to get key per job with `-c` option.

keyConfig file must indicate used parameters per job name as 

```json
{
    "optical_analysis": ["ra", "dec", "radius"]
}
```

A _star_ ( `*` ) means it apply to all jobs


```json
{
    "*": ["ra", "dec", "radius"]
}
```




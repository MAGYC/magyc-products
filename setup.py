import setuptools
from pathlib import Path

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

with open("requirements.txt", "r") as f:
    reqs = f.readlines()



setuptools.setup(
    name="magyc-products", # Replace with your own username
    version="0.0.1",
    author="Marc DEXET",
    author_email="marcdexet (at) universite-paris-saclay.fr",
    description="Toolset for MAGYC products",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://git.ias.u-psud.fr/MAGYC/magyc-products",
    packages=setuptools.find_packages("magycli", "magyc/*"),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    entry_points='''
        [console_scripts]
        magycli=magycli:cli
    ''',
    python_requires='>=3.6',
    install_requires= reqs
)
